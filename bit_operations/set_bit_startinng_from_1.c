#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	unsigned int value ;
	unsigned int pos ;

	printf("Enter the value : ");
	scanf("%u", &value);
	printf("Enter the bit position you want to set : ");
	scanf("%u", &pos);

	printf("Before setting %d bit value is : %d\n", pos, value);

	value = (value | (1UL << pos - 1));
	printf("After setting %d bit value is : %d", pos, value);

}