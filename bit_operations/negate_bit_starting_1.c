#include <stdio.h>
#include <stdlib.h>

unsigned int value;
unsigned int pos;

int main()
{
	printf("Enter the value :");
	scanf("%u", &value);
	printf("Enter the pos you want to negate : ");
	scanf("%u", &pos);

	printf("before negation value is %u\n", value);
	value = (value ^ (1UL << pos -1));
	printf("After negation value is %u\n", value);
	exit(0);

}