#include <stdio.h>
#include <stdlib.h>

unsigned int val;
unsigned int key;
int temp;

int main()
{
	printf("Enter the value of val:");
	scanf("%u", &val);
	printf("Enter the value of key :");
	scanf("%u", &key);
	printf("key = %u\n", key);
	printf("value of val before operation: %u\n", val);
	temp = ~key;
	//printf("temp = %d", temp);
	val = (val & temp);
	printf("value of val after operation : %u\n", val);
	exit(0);

}