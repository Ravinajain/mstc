#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lPCmdLine, int nShowCmd)
{
    static TCHAR szClassName[] = TEXT("The standard window");
    static TCHAR szWindowCaption[] = TEXT("All Group Names");

    HBRUSH hBrush = NULL;
    HCURSOR hCursor = NULL;
    HICON hIcon = NULL;
    HICON hIconSm = NULL;
    HWND hWnd = NULL;

    WNDCLASSEX wndEx;
    MSG msg;

    ZeroMemory(&wndEx, sizeof(WNDCLASSEX));
    ZeroMemory(&msg, sizeof(MSG));

    hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
    if(hBrush == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to obtain brush"),TEXT("GetStockObject"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }
    hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
    if(hCursor == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to obtain cursor"), TEXT("LoadCursor"), MB_ICONERROR);
        return(EXIT_FAILURE);
    } 
    hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
    if(hIcon == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to obtain Icon"), TEXT("LoadIcon"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }
    hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
    if(hIconSm == NULL)
    {
        MessageBox((HWND)NULL, TEXT("Failed to obtain SmallIcon"), TEXT("LoadIcon"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    wndEx.cbSize = sizeof(WNDCLASSEX);
    wndEx.cbClsExtra = 0;
    wndEx.cbWndExtra = 0;
    wndEx.hbrBackground = hBrush;
    wndEx.hCursor = hCursor;
    wndEx.hIcon = hIcon;
    wndEx.hIconSm = hIconSm;
    wndEx.hInstance = hInstance;
    wndEx.lpfnWndProc = WndProc;
    wndEx.lpszClassName = szClassName;
    wndEx.lpszMenuName = NULL;
    wndEx.style = CS_HREDRAW | CS_VREDRAW;

    if(!RegisterClassEx(&wndEx))
    {
        MessageBox((HWND)NULL, TEXT("could not register the class"), TEXT("RegisterClassEx"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }

    hWnd = CreateWindowEx(WS_EX_APPWINDOW, szClassName, szWindowCaption,
                        WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_SYSMENU,
                            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                        (HWND)NULL, (HMENU)NULL, hInstance, NULL);

    if(hWnd == NULL)
    {
        MessageBox((HWND)NULL, TEXT("could not create app window"), TEXT("CreatWindowEx"), MB_ICONERROR);
        return(EXIT_FAILURE);
    }
    ShowWindow(hWnd, nShowCmd);
    UpdateWindow(hWnd);

    {
    while(GetMessage(&msg, (HWND)NULL, 0, 0))
        TranslateMessage(&msg);

        DispatchMessage(&msg);
    }                    
    return(msg.wParam);
}

    LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
    {
        static int cxClient, cyClient, cxChar, cyChar;
        static int arr[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        int i;
        char str[31] = {0};
        int n = 0;

        HDC hdc = NULL;
        TEXTMETRIC tm;
        PAINTSTRUCT ps;

        switch(uMsg)
        {
            case WM_CREATE:
            hdc = GetDC(hWnd);
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth;
            cyChar = tm.tmHeight + tm.tmExternalLeading;
            ReleaseDC(hWnd, hdc);
            hdc = NULL;
            break;

            case WM_SIZE:
            cxClient = LOWORD(lParam);
            cyClient = HIWORD(lParam);
            break;
            
            case WM_PAINT:
            hdc = BeginPaint(hWnd, &ps);
            SetTextAlign(hdc, TA_CENTER | TA_CENTER);
            
            for(i = 0; i < 10; i++)

            {
                //itoa(arr[i], str, 10);
                n += sprintf(&str[n], "%d ", arr[i]);
               

            }
             TextOut(hdc, cxClient/2, cyClient/2, str, lstrlen(str));
            
           

            
            EndPaint(hWnd, &ps);
            hdc = NULL;
            break;

            case WM_DESTROY:
            PostQuitMessage(EXIT_SUCCESS);
            break;
        }

    return(DefWindowProc(hWnd, uMsg, wParam, lParam));
}

    
